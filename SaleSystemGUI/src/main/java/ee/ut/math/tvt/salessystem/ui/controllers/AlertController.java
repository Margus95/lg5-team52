package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import javafx.scene.control.Alert;

public class AlertController {


    public static void alert(String cause){
        Alert alert = new Alert(Alert.AlertType.ERROR);

        switch(cause) {
            case "BarCode":
                alert.setTitle("Error");
                alert.setHeaderText("Wrong input in Bar Code field");
                alert.setContentText("Do not insert bar code that does not exist!\nBar Code can be only integer!");
                alert.showAndWait();
                break;
            case "Price":
                alert.setTitle("Error");
                alert.setHeaderText("Wrong input in Price field");
                alert.setContentText("Price can be only rational number above zero!");
                alert.showAndWait();
                break;
            case "QuantityWH":
                alert.setTitle("Error");
                alert.setHeaderText("Wrong input in Quantity field");
                alert.setContentText("Quantity can be only integer!");
                alert.showAndWait();
                break;
            case "QuantityPOS":
                alert.setTitle("Error");
                alert.setHeaderText("Wrong input in Quantity field");
                alert.setContentText("Quantity can be only integer above zero!");
                alert.showAndWait();
                break;
            case "Limit":
                alert.setTitle("Error");
                alert.setHeaderText("Too many items");
                alert.setContentText("Currently we do not have that quantity of last added item in stock!");
                alert.showAndWait();
                break;
            case "Empty":
                alert.setTitle("Error");
                alert.setHeaderText("Can not submit empty purchase!");
                alert.setContentText("Add atleast 1 item or cancel the purchase!");
                alert.showAndWait();
                break;
            case "Date":
                alert.setTitle("Error");
                alert.setHeaderText("Can not show between chosen dates!");
                alert.setContentText("Start date can not come after End date!");
                alert.showAndWait();
                break;
            default:
                alert.setTitle("Error");
                alert.setHeaderText("Unspecified Error!");
                alert.setContentText("Contact developer team!");
                alert.showAndWait();
                break;
        }

    }
}
