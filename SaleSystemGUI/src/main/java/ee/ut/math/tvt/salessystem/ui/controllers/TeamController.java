package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.ui.SalesSystemUI;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.ResourceBundle;

public class TeamController implements Initializable{
    private static final Logger log = LogManager.getLogger(TeamController.class);


    @FXML
    private Text teamName = new Text();
    private String teamname;
    @FXML
    private Text teamLeader;
    private String teamleader;
    @FXML
    private Text email;
    private String mail;
    @FXML
    private Text teamMembers;
    private String members;
    @FXML
    private String url;
    @FXML
    private ImageView logo;

    public TeamController() throws IOException {

        String filePath = "SaleSystemGUI/src/main/resources/application.properties";
        log.info("Loading information from " + filePath);
        Properties pros = new Properties();


        InputStream ip = getClass().getClassLoader().getResourceAsStream("application.properties");

        if (ip != null) {

            pros.load(ip);
            teamname = pros.getProperty("Team.name");
            teamleader = pros.getProperty("Team.leader");
            mail = pros.getProperty("Team.leader_email");
            members = pros.getProperty("Team.member");
            url = pros.getProperty("Image.URL");
            ip.close();
        }


    }

    @Override
    public void initialize(URL location, ResourceBundle resources)  {
        this.teamName.setText(teamname);
        this.teamLeader.setText(teamleader);
        this.email.setText(mail);
        this.teamMembers.setText(members);
        this.logo.setImage(new Image(url));
        log.info("Team tab is fulled successfully");
    }

}
