package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import javafx.collections.FXCollections;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

/**
 * Encapsulates everything that has to do with the purchase tab (the tab
 * labelled "History" in the menu).
 */
public class HistoryController implements Initializable{

    private final SalesSystemDAO dao;
    private static final Logger log = LogManager.getLogger(HistoryController.class);

    @FXML
    private Button betweenDatesButton;
    @FXML
    private Button last10Button;
    @FXML
    private Button allButton;
    @FXML
    private TableView<Purchase> historyTableViewPurchases;
    @FXML
    private TableView<SoldItem> historyTableViewItems;
    @FXML
    private DatePicker startDate;
    @FXML
    private DatePicker endDate;

    @FXML
    protected void betweenDatesButtonClicked() {
        if(startDate.getValue()==null || endDate.getValue()==null){
            log.info("Date value was not chosen!");
            historyTableViewItems.setItems(FXCollections.observableList(new ArrayList<>()));
            historyTableViewItems.refresh();
        }
        else if(startDate.getValue().isAfter(endDate.getValue())){
            AlertController.alert("Date");
            log.debug("Chosen wrong date! Start date " + startDate.getValue() + " can not be after end date " + endDate.getValue());
            startDate.setValue(null);
            endDate.setValue(null);
            historyTableViewItems.setItems(FXCollections.observableList(new ArrayList<>()));
            historyTableViewItems.refresh();
        }
        else {
            log.debug("Showing purchases between " + startDate.getValue() + " and " + endDate.getValue());
            Collections.sort(dao.findPurchases(), new Comparator<Purchase>() {
                public int compare(Purchase o1, Purchase o2) {
                    LocalDateTime dateTime1 = LocalDateTime.of(o1.getDate(), o1.getTime());
                    LocalDateTime dateTime2 = LocalDateTime.of(o2.getDate(), o2.getTime());
                    return dateTime1.compareTo(dateTime2);
                }
            });
            ArrayList<Purchase> betweenDates = new ArrayList<>();
            for (Purchase purchase : dao.findPurchases()) {
                if ((purchase.getDate().isBefore(endDate.getValue()) || purchase.getDate().isEqual(endDate.getValue()) )
                        &&
                        (purchase.getDate().isAfter(startDate.getValue())) || purchase.getDate().isEqual(startDate.getValue())) {
                    betweenDates.add(purchase);
                }
            }
            historyTableViewPurchases.setItems(FXCollections.observableList(betweenDates));
            historyTableViewPurchases.refresh();
        }
    }

    @FXML
    protected void last10ButtonClicked() {
        log.info("Showing last 10 purchases");
        Collections.sort(dao.findPurchases(), new Comparator<Purchase>() {
            public int compare(Purchase o1, Purchase o2) {
                LocalDateTime dateTime1 = LocalDateTime.of(o1.getDate(), o1.getTime());
                LocalDateTime dateTime2 = LocalDateTime.of(o2.getDate(), o2.getTime());
                return dateTime1.compareTo(dateTime2);
            }
        });
        historyTableViewPurchases.setItems(FXCollections.observableList(dao.findPurchases().subList(0,dao.findPurchases().size())));
        historyTableViewPurchases.refresh();
    }

    @FXML
    protected void rowClicked() {
        historyTableViewItems.setItems(FXCollections.observableList(historyTableViewPurchases.getSelectionModel().getSelectedItem().getItems()));
        historyTableViewItems.refresh();
        System.out.println(historyTableViewPurchases.getSelectionModel().getSelectedItem().toString());
    }

    @FXML
    protected void allButtonClicked() {
        log.info("Showing all purchases");
        historyTableViewPurchases.setItems(FXCollections.observableList(dao.findPurchases()));
        historyTableViewPurchases.refresh();


    }

    public HistoryController(SalesSystemDAO dao) {
        this.dao = dao;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        historyTableViewPurchases.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                rowClicked();
            }
        });
    }


}
