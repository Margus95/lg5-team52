package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.DAOcontroller;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.DAOcontroller;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

public class StockController implements Initializable {

    Alert alert = new Alert(Alert.AlertType.ERROR);

    // DAO variables ---------------------------------------------------------------------------------------------------

    private static final Logger log = LogManager.getLogger(StockController.class);
    private String lastAction;
    private final SalesSystemDAO dao;
    private final DAOcontroller DAOcontroller = new DAOcontroller();

    // FXML variables --------------------------------------------------------------------------------------------------

    @FXML
    private TableView<StockItem> warehouseTableView;
    @FXML
    private Text error;
    @FXML
    private Button addProduct, search, changeProduct, cancelButton;
    @FXML
    private Button addItemButton, deleteItem, changeItem;
    @FXML
    private TextField whBarCodeField, whQuantityField, whNameField, whPriceField;

    // CLASS START METHODS ---------------------------------------------------------------------------------------------

    public StockController(SalesSystemDAO dao) {
        this.dao = dao;
    }
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        refreshStockItems();
        initialInputs();
        // TODO refresh view after adding new items
    }

    // EVENT HANDLERS --------------------------------------------------------------------------------------------------
    // Main section

    @FXML
    public void addProductButtonClicked(){
        lastAction = "Add product";
        log.info("Adding new product. Process started");
        try {
            newProductInputs();
        } catch (SalesSystemException e) {
            log.error(e.getMessage(), e);
        }
    }
    @FXML
    public void searchButtonClicked(){
        lastAction = "Search";
        log.info("Searching for item");
        searchProductInputs();
    }
    @FXML
    public void changeProductButtonClicked(){
        lastAction = "Change product";
        log.info("Changing existing product");
        existingProductInputs();
    }
    @FXML
    public void cancelButtonClicked(){
        log.info("Warehouse action '" + lastAction + "' canceled");
        initialInputs();
    }

    // Lower section

    @FXML
    public void addItemButtonClicked() {
        addStockItems(); //logger added
    }
    @FXML
    public void changeItemButtonClicked(){
        Long id = Long.parseLong(textFieldValues().get(whBarCodeField));
        try {
            double newPrice = Double.parseDouble(textFieldValues().get(whPriceField));
            if (stockItemExists(id) && DAOcontroller.correctPrice(newPrice)) {
                StockItem changed = dao.findStockItem(id);
                log.debug("Changing product with id " + id + ", named " + changed.getName() + ". Price changed from " + changed.getPrice() + " to " + newPrice);
                DAOcontroller.changeStockPrice(dao, id, newPrice);
                resetWhField();
                refreshStockItems();
            } else {
                resetWhField();
            }
        } catch (Exception e) {
            AlertController.alert("Price");
            e.printStackTrace();
            resetWhField();
        }
    }
    @FXML
    public void deleteItemButtonClicked(){
        try {
            Long id = Long.parseLong(textFieldValues().get(whBarCodeField));
            stockItemExists(id);
            StockItem deletable = dao.findStockItem(id);
            dao.deleteStockItem(deletable);
            log.debug("Deleting " + deletable);
            refreshStockItems();
            resetWhField();
        } catch (Exception e) {
            e.printStackTrace();
            resetWhField();
        }
    }
    @FXML
    public void refreshButtonClicked() {
        refreshStockItems(); //logger added
    }

    // LOGIC -----------------------------------------------------------------------------------------------------------

    private boolean stockItemExists(Long id) {
        if (DAOcontroller.itemExists(dao, id)) {
            return true;
        } else return false;
    }

    private void showSearchedBySelectedStockItem() {
        try {
            Long id = Long.parseLong(textFieldValues().get(whBarCodeField));
            stockItemExists(id);
            StockItem searched = dao.findStockItem(id);
            whNameField.setText(searched.getName());
            whPriceField.setText(String.valueOf(searched.getPrice()));
            whQuantityField.setText(String.valueOf(searched.getQuantity()));
            log.debug("Found " + searched + " for action " + lastAction);
        } catch (Exception e) {
            e.printStackTrace();
            resetWhField();
        }
    }

    private void fillInputsBySelectedStockItem() {
        try {
            if(!textFieldValues().get(whBarCodeField).isBlank()) {
                Long id = Long.parseLong(textFieldValues().get(whBarCodeField));
                stockItemExists(id);
                StockItem selected = dao.findStockItem(id);
                whNameField.setText(selected.getName());
                whPriceField.setText(String.valueOf(selected.getPrice()));
            }
        } catch (Exception e) {
            AlertController.alert("BarCode");
            e.printStackTrace();
            resetWhField();
        }
    }

    private void addStockItems() {
        try {
            String newName = textFieldValues().get(whNameField);
            try {
                double newPrice = Double.parseDouble(textFieldValues().get(whPriceField));
                if(newPrice < 0)
                    throw new SalesSystemException("Please enter new Quantity!");
            }
            catch (Exception e){
                AlertController.alert("Price");
                whPriceField.setText("");
                return;
            }
            try {
                int newQuantity = Integer.parseInt(textFieldValues().get(whQuantityField));
            }
            catch (Exception e){
                AlertController.alert("QuantityWH");
                whQuantityField.setText("");
                return;
            }
            double newPrice = Double.parseDouble(textFieldValues().get(whPriceField));
            int newQuantity = Integer.parseInt(textFieldValues().get(whQuantityField));
            if (whBarCodeField.getText().isBlank()) {
                Long id = DAOcontroller.getMaxID(dao) + 1L;
                DAOcontroller.correctAllInput(id, newName, newPrice, newQuantity);
                DAOcontroller.addNewItem(dao, id, newName, newPrice, newQuantity);
                log.debug("New barcode generated - " + id + ". Added " + dao.findStockItem(id));
                refreshStockItems();
                resetWhField();
            } else {
                Long id = Long.parseLong(textFieldValues().get(whBarCodeField));
                DAOcontroller.addStockQuantity(dao, id, newQuantity);
                refreshStockItems();
                resetWhField();
            }
        } catch (Exception e) {
            e.printStackTrace();
            resetWhField();
        }
    }

    // GUI ELEMENT STATE -----------------------------------------------------------------------------------------------

    private void refreshStockItems() {
        warehouseTableView.setItems(FXCollections.observableList(dao.findStockItems()));
        warehouseTableView.refresh();
        log.info("Warehouse view refreshed");
    }

    private void resetWhField() {
        whBarCodeField.setText("");
        whQuantityField.setText("");
        whNameField.setText("");
        whPriceField.setText("");
    }

    private HashMap<TextField, String> textFieldValues() {
        HashMap<TextField, String> values = new HashMap<>();
        values.put(whBarCodeField, whBarCodeField.getText());
        values.put(whQuantityField, whQuantityField.getText());
        values.put(whNameField, whNameField.getText());
        values.put(whPriceField, whPriceField.getText());
        log.info("TextField values saved");
        return values;
    }

    private void initialInputs(){
        resetWhField();
        //-----------------------------
        addProduct.setDisable(false);
        search.setDisable(false);
        changeProduct.setDisable(false);
        cancelButton.setDisable(true);
        //-----------------------------
        changeItem.setDisable(true);
        addItemButton.setDisable(true);
        deleteItem.setDisable(true);
        //-----------------------------
        whBarCodeField.setDisable(true);
        whQuantityField.setDisable(true);
        whPriceField.setDisable(true);
        whNameField.setDisable(true);
    }

    private void searchProductInputs() {
        resetWhField();
        //-----------------------------
        addProduct.setDisable(true);
        search.setDisable(true);
        changeProduct.setDisable(true);
        cancelButton.setDisable(false);
        //-----------------------------
        changeItem.setDisable(true);
        addItemButton.setDisable(true);
        deleteItem.setDisable(true);
        //-----------------------------
        whBarCodeField.setDisable(false);
        whQuantityField.setDisable(true);
        whPriceField.setDisable(true);
        whNameField.setDisable(true);

        this.whBarCodeField.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if (!newPropertyValue) {
                    showSearchedBySelectedStockItem();
                }
            }
        });
    }

    private void existingProductInputs(){
        resetWhField();
        //-----------------------------
        addProduct.setDisable(true);
        search.setDisable(true);
        changeProduct.setDisable(true);
        cancelButton.setDisable(false);
        //-----------------------------
        changeItem.setDisable(false);
        addItemButton.setDisable(true);
        deleteItem.setDisable(false);
        //-----------------------------
        whBarCodeField.setDisable(false);
        whQuantityField.setDisable(true);
        whPriceField.setDisable(false);
        whNameField.setDisable(false);

        this.whBarCodeField.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if (!newPropertyValue) {
                    fillInputsBySelectedStockItem();
                }
            }
        });
    }

    public void newProductInputs(){
        resetWhField();
        //-----------------------------
        addProduct.setDisable(true);
        search.setDisable(true);
        changeProduct.setDisable(true);
        cancelButton.setDisable(false);
        //-----------------------------
        changeItem.setDisable(true);
        addItemButton.setDisable(false);
        deleteItem.setDisable(true);
        //-----------------------------
        whBarCodeField.setDisable(false);
        whQuantityField.setDisable(false);
        whPriceField.setDisable(false);
        whNameField.setDisable(false);

        this.whBarCodeField.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if (!newPropertyValue) {
                    fillInputsBySelectedStockItem();
                }
            }
        });
    }

}
