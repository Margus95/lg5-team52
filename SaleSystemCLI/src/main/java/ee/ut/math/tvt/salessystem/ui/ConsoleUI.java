package ee.ut.math.tvt.salessystem.ui;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.DAOcontroller;
import ee.ut.math.tvt.salessystem.dao.HibernateSalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.Scanner;

/**
 * A simple CLI (limited functionality).
 */
public class ConsoleUI {
    private static final Logger log = LogManager.getLogger(ConsoleUI.class);

    private final SalesSystemDAO dao;
    private final ShoppingCart cart;

    public ConsoleUI(SalesSystemDAO dao) {
        this.dao = dao;
        cart = new ShoppingCart(dao);
    }

    public static void main(String[] args) throws Exception {
        SalesSystemDAO dao = new InMemorySalesSystemDAO();
        ConsoleUI console = new ConsoleUI(dao);
        console.run();
    }

    /**
     * Run the sales system CLI.
     */
    public void run() throws IOException {
        log.info("Starting CLI sales system");
        System.out.println("===========================");
        System.out.println("=       Sales System      =");
        System.out.println("===========================");
        printUsage();
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            System.out.print("> ");
            processCommand(in.readLine().trim().toLowerCase());
            System.out.println("Done. ");
        }
    }

    private void showStock() {
        log.info("Warehouse info shown");
        List<StockItem> stockItems = dao.findStockItems();
        System.out.println("-------------------------");
        for (StockItem si : stockItems) {
            System.out.println(si.getId() + " " + si.getName() + " " + si.getPrice() + "Euro (" + si.getQuantity() + " items)");
        }
        if (stockItems.size() == 0) {
            System.out.println("\tNothing");
        }
        System.out.println("-------------------------");
    }

    private void showCart() {
        log.info("Shopping cart info shown");
        System.out.println("-------------------------");
        for (SoldItem si : cart.getAll()) {
            System.out.println(si.getName() + " " + si.getPrice() + "Euro (" + si.getQuantity() + " items)");
        }
        if (cart.getAll().size() == 0) {
            System.out.println("\tNothing");
        }
        System.out.println("-------------------------");
    }

    private void printUsage() {
        log.info("Controls info shown");
        System.out.println("-------------------------");
        System.out.println("Usage:");
        System.out.println("h\t\tShow this help");
        System.out.println("w\t\tShow warehouse contents");
        System.out.println("s NAME NR PR \tAdd NR of stock item named NAME with price PR to stock");
        System.out.println("c\t\tShow cart contents");
        System.out.println("a IDX NR \tAdd NR of stock item with index IDX to the cart");
        System.out.println("p\t\tPurchase the shopping cart");
        System.out.println("r\t\tReset the shopping cart");
        System.out.println("t\t\tShow team info");
        System.out.println("l10\t\tShow last 10 purchases");
        System.out.println("-------------------------");
    }

    private void processCommand(String command) {
        String[] c = command.split(" ");

        //log.debug(" Input = '" + command + "'");
        if (c[0].equals("h"))
            printUsage();
        else if (c[0].equals("q"))
            System.exit(0);
        else if (c[0].equals("w"))
            showStock();
        else if (c[0].equals("c"))
            showCart();
        else if (c[0].equals("p"))

            cart.submitCurrentPurchase();
        else if (c[0].equals("r"))
            cart.cancelCurrentPurchase();
        else if (c[0].equals("a") && c.length == 3) {
            try {
                long idx = Long.parseLong(c[1]);
                int amount = Integer.parseInt(c[2]);
                StockItem item = dao.findStockItem(idx);
                if (item != null) {
                    cart.addItem(new SoldItem(item, Math.min(amount, item.getQuantity())));
                } else {
                    System.out.println("no stock item with id " + idx);
                }
            } catch (SalesSystemException | NoSuchElementException e) {
                log.error(e.getMessage(), e);
            }
        }else if (c[0].equals("s") && c.length == 4){
            try{
                String name = c[1];
                int amount = Integer.parseInt(c[2]);
                double price = Double.parseDouble(c[3]);
                StockItem newStockItem = new StockItem(DAOcontroller.getMaxID(dao)+1, name, "blank", price, amount);
                dao.saveStockItem(newStockItem);
                log.info("Product '"+newStockItem.getName()
                        +"' with price " + newStockItem.getPrice()
                        +" in quantity of " + newStockItem.getQuantity()
                        + " added to the stock");
            } catch (SalesSystemException | NoSuchElementException e) {
                log.error(e.getMessage(), e);
            }
        }else if(c[0].equals("l10")) {
            showLast10Purchases();
        }else if (c[0].equals("t")) {
            showTeam();
        } else {
            log.info("unknown command");
            System.out.println("unknown command");
        }
    }

    private void showLast10Purchases(){
        List<Purchase> purchases = dao.findPurchases();
        System.out.println("ID\tDate\t\tTime\t\tTotal");
        for (int i = 0; i < 10 && i<purchases.size(); i++) {
            System.out.println(purchases.get(i).getId()+"\t"
                    +purchases.get(i).getDate()+"\t"
                    +(purchases.get(i).getTime().toString()).subSequence(0,8)+"\t"
                    +purchases.get(i).getTotal());

        }

    }

    private void showTeam() {
        String filePath = "SaleSystemGUI/src/main/resources/application.properties";

        Properties pros = new Properties();
        try (FileInputStream ip = new FileInputStream(filePath)) {
            pros.load(ip);
            System.out.println("TEAM NAME: " + pros.get("Team.name"));
            System.out.println("TEAM LEADER: " + pros.get("Team.leader"));
            System.out.println("TEAM LEADER EMAIL: " + pros.get("Team.leader_email"));
            System.out.println("TEAM MEMBERS: " + pros.get("Team.member"));
            System.out.println("----------------------------------");
            Scanner sc = new Scanner(System.in);
            System.out.println("Would you like to continue? (y/n)");
            String input = sc.nextLine();
            if (input.equalsIgnoreCase("y")) {
                log.info("Continue with input 'y'");
                SalesSystemDAO dao = new InMemorySalesSystemDAO();
                ConsoleUI console = new ConsoleUI(dao);
                console.run();
            } else {
                //log.debug("Input was '" + input + "'. System shut down");
                System.exit(0);
            }
        } catch (IOException e) {
            log.error(e.getMessage() + "Data was not found! The program will be shut down!");
            System.out.println("Data was not found! The program will be shut down!");
            System.exit(1);
        }
   }

}
