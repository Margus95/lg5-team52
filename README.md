# Team <LG5-Team52>:
1. <Margus Štšjogolev>
2. <Andrei Tambovtsev>
3. <>

## Homework 1:
<https://bitbucket.org/Margus95/lg5-team52/wiki/Assignment%201> 

## Homework 2:
https://bitbucket.org/Margus95/lg5-team52/wiki/Assignment%202

## Homework 3:
https://bitbucket.org/Margus95/lg5-team52/commits/tag/homework3

## Homework 4:
https://bitbucket.org/Margus95/lg5-team52/wiki/Assignment%204

## Homework 5:
https://bitbucket.org/Margus95/lg5-team52/wiki/Assignment%205

## Homework 6:
https://bitbucket.org/Margus95/lg5-team52/wiki/Assignment%206

## Homework 7:
<Links to the solution>

We encourage you to use [markdown syntax](https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)