package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

public class HibernateSalesSystemDAO implements SalesSystemDAO {

    private static final Logger log = LogManager.getLogger(HibernateSalesSystemDAO.class);


    private final EntityManagerFactory emf;
    private final EntityManager em;

    public HibernateSalesSystemDAO() {
        // if you get ConnectException/JDBCConnectionException then you
        // probably forgot to start the database before starting the application
        emf = Persistence.createEntityManagerFactory("pos");
        em = emf.createEntityManager();
    }

    // TODO implement missing methods

    public void close() {
        em.close();
        emf.close();
    }


    public List<StockItem> getStockItems() {
        return em.createQuery("from StockItem", StockItem.class).getResultList();
    }
    public List<SoldItem> getSoldItems() {
        return em.createQuery("from SoldItem", SoldItem.class).getResultList();
    }
    public List<Purchase> getPurchases() {
        return em.createQuery("from Purchase", Purchase.class).getResultList();
    }


    @Override
    public List<StockItem> findStockItems() {
        return getStockItems();
    }

    @Override
    public StockItem findStockItem(long id) {
        for (StockItem item : findStockItems()) {
            if (item.getId() == id)
                return item;
        }
        return null;
    }

    @Override
    public Purchase findPurchase(Long id) {
        for (Purchase purchase : findPurchases()) {
            if (purchase.getId() == id)
                return purchase;
        }
        return null;
    }

    @Override
    public List<Purchase> findPurchases() {
        return getPurchases();
    }

    @Override
    public void saveStockItem(StockItem stockItem) {

        beginTransaction();
        log.info("transaction to DB begins");
        log.info(stockItem);

        stockItem.setId(null);
        em.persist(stockItem);
        em.flush();

        /* UPDATE
        Query query = em.createQuery("Update StockItem set id = " + stockItem.getId() +
                ", name = '" + stockItem.getName() + "', price = " + stockItem.getPrice() + ", quantity = " + stockItem.getQuantity() +
                ", description = '" + stockItem.getDescription() + "'");
        query.executeUpdate();
         */
        log.info("updating");
        commitTransaction();

        log.info("updated");
    }

    @Override
    public void saveSoldItem(SoldItem soldItem) {
        beginTransaction();
        log.info("transaction to DB begins");
        soldItem.setId(null);
        em.persist(soldItem);
        em.flush();
        log.info("updating");
        commitTransaction();

        log.info("updated");
    }

    @Override
    public void deleteStockItem(StockItem stockItem) {
        beginTransaction();
        log.info("transaction to DB begins");
        em.remove(stockItem);
        em.flush();
        log.info("updating");
        commitTransaction();

        log.info("updated");
    }

    @Override
    public void savePurchase(Purchase purchase) {
        beginTransaction();
        log.info("transaction to DB begins");
        purchase.setId(null);
        em.persist(purchase);
        em.flush();
        log.info("updating");
        commitTransaction();

        log.info("updated");
    }

    @Override
    public void beginTransaction() {
        em.getTransaction().begin();
    }

    @Override
    public void rollbackTransaction() {
        em.getTransaction().rollback();
    }

    @Override
    public void commitTransaction() {
        em.getTransaction().commit();
    }
}