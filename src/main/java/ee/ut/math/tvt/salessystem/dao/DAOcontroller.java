package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.security.spec.ECField;

public class DAOcontroller {

    // This class is used to manipulate data in the SaleSystemDao
    // This class is not used to put new data or retrieve data
    // This class uses save/retrieve methods from the InMemorySaleSystemDAO class

    private static final Logger log = LogManager.getLogger(DAOcontroller.class);

    public boolean itemExists(SalesSystemDAO dao,Long id) {
        if (dao.findStockItem(id) != null) {
            return true;
        } else {
            throw new SalesSystemException("Item does not exist!");
        }
    }

    public void changeStockPrice(SalesSystemDAO dao,Long id, double newPrice) {
        if (itemExists(dao, id)) {
            dao.findStockItem(id).setPrice(newPrice);
        }
    }

    public void addStockQuantity(SalesSystemDAO dao,Long id, int additionalQuantity) {
        if (itemExists(dao, id)) {
            int realQuantity = dao.findStockItem(id).getQuantity();
            dao.findStockItem(id).setQuantity(realQuantity + additionalQuantity);
        }
    }

    public static Long getMaxID(SalesSystemDAO dao) {
        Long maxID = 0L;
        for (StockItem element : dao.findStockItems()) {
            if (element.getId() > maxID) {
                maxID = element.getId();
            }
        }
        return maxID;
    }

    public static Long getPurchaseMaxID(SalesSystemDAO dao) {
        Long maxID = 0L;
        for (Purchase element : dao.findPurchases()) {
            if (element.getId() > maxID) {
                maxID = element.getId();
            }
        }
        return maxID;
    }

    public boolean correctPrice(double enteredPrice) {
        if (enteredPrice < 0.0) {
            throw new SalesSystemException("Entered price cannot be a negative number!");
        } else return true;
    }

    public boolean correctQuantity(int enteredQuantity) {
       if (enteredQuantity < 0) {
            throw new SalesSystemException("Entered quantity cannot be a negative number!");
       } else return true;
    }

    public boolean correctName(String enteredName) {
        if (enteredName.isBlank() || enteredName.isEmpty()) {
            throw new SalesSystemException("Please enter new name!");
        } else return true;
    }

    public boolean correctID(Long enteredID) {
        if (enteredID < 0L) {
            throw new SalesSystemException("Entered ID cannot be negative!");
        } else return true;
    }

    public boolean correctAllInput(Long ID, String name, double price, int quantity) {
        if (correctID(ID) && correctName(name) && correctPrice(price) && correctQuantity(quantity))
            return true;
        else throw new SalesSystemException("Incorrect input! Please check name, price and quantity!");
    }

    public void addNewItem(SalesSystemDAO dao, Long ID, String name, double price, int quantity) {
        try {
            correctAllInput(ID, name, price, quantity);
            if (itemExists(dao, ID))
                addStockQuantity(dao, ID, quantity);
        } catch (Exception e) {
            StockItem newStockItem = new StockItem(getMaxID(dao)+1, name, "blank", price, quantity);
            dao.saveStockItem(newStockItem);
            log.debug("New barcode generated - " + newStockItem.getId() + ". Added " + newStockItem);
        }
    }








}
