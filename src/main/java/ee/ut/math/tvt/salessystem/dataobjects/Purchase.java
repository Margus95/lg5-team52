package ee.ut.math.tvt.salessystem.dataobjects;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

/**
Set of sold Items
 */
@Entity
@Table(name = "PURCHASE")
public class Purchase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToMany
    @JoinTable(name = "PURCHASES_TO_SOLDITEM",
            joinColumns = @JoinColumn(name = "PURCHASE_ID", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "SOLDITEM_ID", referencedColumnName = "ID"))
    private List<SoldItem> items;

    @Column(name = "date")
    private LocalDate date;

    @Column(name = "time")
    private LocalTime time;

    @Column(name = "total")
    private double total;

    public Purchase(Long id, List<SoldItem> items) {
        this.id = id;
        this.date = LocalDate.now();
        this.time = LocalTime.now();
        this.items = items;
        calculateTotal(items);
    }

    public void calculateTotal(List<SoldItem> items) {
        for (SoldItem item : items) {
            this.total += item.getTotal();
        }
    }

    public Purchase() {
        this.date = null;
        this.time = null;
        this.items = new ArrayList<>();
    }

    public List<SoldItem> getItems() {
        return items;
    }

    public void setItems(List<SoldItem> items) {
        this.items = items;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Purchase{" +
                "items=" + items +
                ", date=" + date +
                ", time=" + time +
                '}';
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getTotal() {
        return total;
    }
}
