package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class InMemorySalesSystemDAO implements SalesSystemDAO {

    private static final Logger log = LogManager.getLogger(InMemorySalesSystemDAO.class);


    // This class is used for new data saving and retrieval
    // This class can not be used for data manipulation

    private final List<StockItem> stockItemList;
    private final List<SoldItem> soldItemList;
    private final List<Purchase> purchases;
    private int TransCalled = 0;

    public InMemorySalesSystemDAO() {
        List<StockItem> items = new ArrayList<StockItem>();
        items.add(new StockItem(1L, "Lays chips", "Potato chips", 11.0, 5));
        items.add(new StockItem(2L, "Chupa-chups", "Sweets", 8.0, 8));
        items.add(new StockItem(3L, "Frankfurters", "Beer sauseges", 15.0, 12));
        items.add(new StockItem(4L, "Free Beer", "Student's delight", 0.0, 100));
        this.stockItemList = items;
        this.soldItemList = new ArrayList<>();
        this.purchases = new ArrayList<>();
    }

    @Override
    public List<StockItem> findStockItems() {
        return stockItemList;
    }

    @Override
    public StockItem findStockItem(long id) {
        for (StockItem item : stockItemList) {
            if (item.getId() == id)
                return item;
        }
        return null;
    }

    @Override
    public Purchase findPurchase(Long id) {
        for (Purchase purchase : purchases) {
            if (purchase.getId() == id)
                return purchase;
        }
        return null;
    }

    @Override
    public List<Purchase> findPurchases() {
        return this.purchases;
    }

    @Override
    public void saveSoldItem(SoldItem item) {
        beginTransaction();
        soldItemList.add(item);
        commitTransaction();
    }

    @Override
    public void savePurchase(Purchase purchase) {
        this.purchases.add(purchase);
    }

    @Override
    public void deleteStockItem(StockItem item){
        if (findStockItem(item.getId())!=null)
            stockItemList.remove(item);
        else throw new SalesSystemException("This item does not exist!");
    }

    @Override
    public void saveStockItem(StockItem stockItem) {
        if (stockItem.getQuantity() >= 0 && stockItem.getPrice() >= 0)
            stockItemList.add(stockItem);
        else throw new SalesSystemException("Quantity or price cannot be negative!");
    }

    @Override
    public void beginTransaction() {
        this.TransCalled =+ 1;
        Purchase currentCart =  new Purchase();
    }

    @Override
    public void rollbackTransaction() {
    }

    @Override
    public void commitTransaction() {
      this.TransCalled =- this.TransCalled;
    }

    public List<SoldItem> findSoldItems() {
        return this.soldItemList;
    }

    public int getTransCalled() {
        return TransCalled;
    }
}
