package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.DAOcontroller;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {
    private static final Logger log = LogManager.getLogger(ShoppingCart.class);

    private final SalesSystemDAO dao;
    private final List<SoldItem> items = new ArrayList<>();

    public ShoppingCart(SalesSystemDAO dao) {
        this.dao = dao;
    }

    /**
     * Add new SoldItem to table.
     */

    public List<SoldItem> getAll() {
        return items;
    }

    public void cancelCurrentPurchase() {
        log.debug("Transaction with " + items + " canceled");
        items.clear();
    }

    public void addItem(SoldItem item) {
        // TODO In case such stockItem already exists increase the quantity of the existing stock
        // TODO verify that warehouse items' quantity remains at least zero or throw an exception

        if(canBeAdded(item) && alreadyInCart(item))
            for(SoldItem itemInCart : items){
                if (itemInCart.getId() == item.getId()){
                    itemInCart.setQuantity(itemInCart.getQuantity()+item.getQuantity());
                    itemInCart.setTotal(itemInCart.getQuantity()*itemInCart.getPrice());
                    log.debug(item.getName() + " quantity in cart increased by " + item.getQuantity());
                }
            }
        else if(canBeAdded(item) && !alreadyInCart(item)){
            items.add(item);
            log.debug("Added " + item.getName() + " quantity of " + item.getQuantity());
        }

        else if(!canBeAdded(item)){
            log.debug(item.getName() + " can not be added in quantity of " + item.getQuantity());
            throw new SalesSystemException("can not be added");
        }


    }

    public boolean alreadyInCart(SoldItem item){
        if(items.size() == 0){
            return false;
        }
        for (SoldItem itemInCart : items) {
            if (itemInCart.getId() == item.getId()){
                return true;
            }
        }
        return false;
    }

    public boolean canBeAdded(SoldItem item) {
        long purchasingItemId = item.getId();
        StockItem purchasingItemInStock = dao.findStockItem(purchasingItemId);

        if(!alreadyInCart(item)){
            if(item.getQuantity() > purchasingItemInStock.getQuantity()){
                return false;
            }else{
                return true;
            }
        } else{
            for (SoldItem itemInCart : items) {
                if (itemInCart.getId() == item.getId() && item.getQuantity() + itemInCart.getQuantity()  > purchasingItemInStock.getQuantity()){
                        return false;
                    }
                }
            }
        return true;
    }

    public void submitCurrentPurchase() {
        // TODO decrease quantities of the warehouse stock

        // note the use of transactions. InMemorySalesSystemDAO ignores transactions
        // but when you start using hibernate in lab5, then it will become relevant.
        // what is a transaction? https://stackoverflow.com/q/974596
        try {

            for (SoldItem item : items) {
                StockItem sameItemInStock = dao.findStockItem(item.getId());
                sameItemInStock.setQuantity(sameItemInStock.getQuantity()-item.getQuantity());
                dao.saveSoldItem(item);
            }
            ArrayList<SoldItem> copyOfItems = new ArrayList<>();
            for (SoldItem item : items)
                copyOfItems.add(item);

            dao.savePurchase(new Purchase(DAOcontroller.getPurchaseMaxID(dao)+1, copyOfItems));
            log.debug("Transaction with " + items + " completed");

            items.clear();
        } catch (Exception e) {
            log.debug("Transaction failed " + getAll().toString());
            log.error(e.getMessage(),e);
            dao.rollbackTransaction();
            throw e;
        }
    }

    public void removeSoldItem(Long id) {
        for (SoldItem item : items) {
            if(item.getId()==id){
                items.remove(item);
                break;
            }
        }
    }


}
