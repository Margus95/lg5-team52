package ee.ut.math.tvt.salessystem.dataobjects;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Already bought StockItem. SoldItem duplicates name and price for preserving history.
 */
@Entity
@Table(name = "SOLDITEM")
public class SoldItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "product_name")
    private String name;

    @Column(name = "quantity")
    private Integer quantity;

    @Column(name = "price")
    private double price;

    @Column(name = "total")
    private double total;

    @ManyToMany(mappedBy = "items")
    private List<Purchase> involvedInPurchases;

    public SoldItem() {
    }

    public List<Purchase> getInvolvedInPurchases() {
        return involvedInPurchases;
    }

    public void setInvolvedInPurchases(List<Purchase> involvedInPurchases) {
        this.involvedInPurchases = involvedInPurchases;
    }

    public SoldItem(StockItem stockItem, int quantity) {
        //this.stockItem = stockItem;
        this.id = stockItem.getId();
        this.name = stockItem.getName();
        this.price = stockItem.getPrice();
        this.quantity = quantity;
        this.total = (double)quantity*price;
        this.involvedInPurchases = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public double getSum() {
        return price * ((double) quantity);
    }


    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    /*
    public List<Purchase> findInvolvedPurchases(){
        List<Purchase> involved = new ArrayList<Purchase>();

    }

     */

    @Override
    public String toString() {
        return String.format("SoldItem{id=%d, name='%s', price = '%s', quantity='%s'}", id, name, price, quantity);
    }
}
