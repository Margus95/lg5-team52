import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.DAOcontroller;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

public class DeleteItemTest {

    private InMemorySalesSystemDAO dao;
    private DAOcontroller DAOcontroller = new DAOcontroller();

    @Before
    public void setUp() {
        this.dao = new InMemorySalesSystemDAO();
    }

    @Test
    public void testRemoveItem() {
        dao.deleteStockItem(dao.findStockItem(1));
        assertEquals(null, dao.findStockItem(1));
    }

    @Test
    public void testRemoveNon() {
        // Test trying to remove a product that is not in the database
        StockItem removable = new StockItem(55L, "Ice", "qg3gq", 1.5, 100);
        try {
            dao.deleteStockItem(removable);
            assertEquals(1, 0);
        } catch (Exception e) {
            assertEquals("This item does not exist!", e.getMessage());
        }
    }
}
