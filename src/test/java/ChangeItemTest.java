import ee.ut.math.tvt.salessystem.dao.DAOcontroller;
import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

public class ChangeItemTest {

    private InMemorySalesSystemDAO dao;
    private DAOcontroller DAOcontroller = new DAOcontroller();

    @Before
    public void setUp() {
        this.dao = new InMemorySalesSystemDAO();
    }

    @Test
    public void priceChangeTest() {
        DAOcontroller.changeStockPrice(dao, 1L, 22.22);
        assertEquals(22.22, dao.findStockItem(1L).getPrice(), 0.001);
    }



}
