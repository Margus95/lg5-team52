import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.DAOcontroller;
import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class SubmitCurrentPurchaseTest {

    private ShoppingCart cart;
    private InMemorySalesSystemDAO dao;

    @Before
    public void setUp() {
        this.dao = new InMemorySalesSystemDAO();
        this.cart = new ShoppingCart(dao);
    }

    @Test
    public void testSubmittingCurrentPurchase() {
        SoldItem soldItem = new SoldItem(dao.findStockItem(1L), 1);
        cart.addItem(soldItem);
        cart.submitCurrentPurchase();
        assertEquals(4, dao.findStockItem(1L).getQuantity());
    }

    @Test
    public void testSubmittingCurrentPurchaseBeginsAndCommitsTransaction() {
        cart.submitCurrentPurchase();
        assertEquals(0, dao.getTransCalled());
    }

    @Test
    public void testSubmittingCurrentOrderCreatesHistoryItem() {
        SoldItem soldItem = new SoldItem(dao.findStockItem(1L), 1);
        cart.addItem(soldItem);
        cart.submitCurrentPurchase();
        assertTrue(dao.findSoldItems().contains(soldItem));
    }

    @Test
    public void testSubmittingCurrentOrderSavesCorrectTime() {
        int minutes = LocalTime.now().getMinute();
        List<SoldItem> soldItems = new ArrayList<>();
        soldItems.add(new SoldItem(dao.findStockItem(1L), 1));
        dao.savePurchase(new Purchase(soldItems));
        assertEquals(minutes, dao.findPurchases().get(0).getTime().getMinute());
    }

    @Test
    public void testCancellingOrder() {
        SoldItem soldItem1 = new SoldItem(dao.findStockItem(1L), 1);
        cart.addItem(soldItem1);
        cart.cancelCurrentPurchase();
        SoldItem soldItem2 = new SoldItem(dao.findStockItem(2L), 1);
        cart.addItem(soldItem2);
        cart.submitCurrentPurchase();
        assertTrue(!dao.findSoldItems().contains(soldItem1));
    }

    @Test
    public void testCancellingOrderQuantitiesUnchanged() {
        SoldItem soldItem1 = new SoldItem(dao.findStockItem(1L), 1);
        cart.addItem(soldItem1);
        cart.cancelCurrentPurchase();
        assertEquals(5, dao.findStockItem(1L).getQuantity());
    }

}
