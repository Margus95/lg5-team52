import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.DAOcontroller;
import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

import java.io.DataOutputStream;
import java.util.ArrayList;

public class AddItemTest {

    private InMemorySalesSystemDAO dao;
    private DAOcontroller DAOcontroller = new DAOcontroller();

    @Before
    public void setUp() {
        this.dao = new InMemorySalesSystemDAO();
    }

    @Test
    public void itemExistsTest() {
        try {
            assertEquals(true, DAOcontroller.itemExists(dao,1L));
        } catch (Exception e) {
            assertEquals(true, false);
        }
        try {
            DAOcontroller.itemExists(dao,99999L);
            assertEquals(true, false);
        } catch (Exception e) {
            assertEquals(true, true);
        }

    }

    @Test
    public void testAddingNewItem() {
        Long id = 123456L;
        try {
            DAOcontroller.addNewItem(dao, id, "name", 12.34, 10);
            try {
                assertEquals(true, DAOcontroller.itemExists(dao, id));
            } catch (Exception e) {
                assertEquals(true, false);
            }
        } catch (Exception e) {
            assertEquals(true, false);
        }
    }

    @Test
    public void testAddingExistingItem() {
        StockItem addingStockItem = new StockItem(1L, "Lays chips", "Potato chips", 11.0, 5);
        try {
            DAOcontroller.itemExists(dao, 1L);
            assertEquals(true, false);
        } catch (Exception e) {
            DAOcontroller.addStockQuantity(dao, 1L, 5);
            assertEquals(10, dao.findStockItem(1L).getQuantity());
        }
    }

    @Test
    public void testAddingItemWithNegativeQuantity() {
        Long id = 123456L;
        StockItem stockItem = new StockItem(id, "ItemTest", "description", 12.34,-2);
        try {
            DAOcontroller.addNewItem(dao, id, "ItemTest", 12.34, -2);
            assertEquals(true, false);
        } catch (Exception e) {
            assertEquals(true, true);
        }
    }

    @Test
    public void testNewAddingItemWithNegativePrice() {
        Long id = 123456L;
        try {
            DAOcontroller.addNewItem(dao, id, "ItemTest", -12.34, 2);
            assertEquals(true, false);
        } catch (Exception e) {
            assertEquals(true, true);
        }
    }

    @Test
    public void maxIDTest() {
        DAOcontroller.addNewItem(dao,999L, "name", 00.00, 1);
        assertEquals(999L, DAOcontroller.getMaxID(dao), 0.001);
    }

    @Test
    public void correctInputTest() {
        try {
            assertEquals(true, DAOcontroller.correctAllInput(1L, "name", 12.23, 15));
        } catch (Exception e) {
            assertEquals(true, false);
        }
    }

    @Test
    public void incorrectInputTest() {
        try {
            DAOcontroller.correctAllInput(-1L, "name", 12.23, 15);
            assertEquals(true, false);
        } catch (Exception e) {
            assertEquals(true, true);
        }
        try {
            DAOcontroller.correctAllInput(1L, "", 12.23, 15);
            assertEquals(true, false);
        } catch (Exception e) {
            assertEquals(true, true);
        }
        try {
            DAOcontroller.correctAllInput(1L, "name", -12.23, 15);
            assertEquals(true, false);
        } catch (Exception e) {
            assertEquals(true, true);
        }
        try {
            DAOcontroller.correctAllInput(1L, "name", 12.23, -15);
            assertEquals(true, false);
        } catch (Exception e) {
            assertEquals(true, true);
        }
    }

}
