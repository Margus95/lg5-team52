import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.DAOcontroller;
import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import org.junit.Before;
import org.junit.Test;

public class AddingToCart {

    private ShoppingCart cart;
    private InMemorySalesSystemDAO dao;

    @Before
    public void setUp() {
        this.dao = new InMemorySalesSystemDAO();
        this.cart = new ShoppingCart(dao);
    }

    @Test
    // 1. testAddingExistingItem - check that adding
    // an existing item increases the quantity
    public void testAddingExistingItem() {
        SoldItem soldItem = new SoldItem(new StockItem(1L, "test", "", 2.0, 2), 2);
        cart.addItem(soldItem);
        cart.addItem(soldItem);
        if (cart.alreadyInCart(soldItem))
            assertEquals(java.util.Optional.of(4), java.util.Optional.of(cart.getAll().get(0).getQuantity()));
        else assertEquals(1, 0);
    }

    @Test
    public void testAddingNewItem() {
        SoldItem soldItem = new SoldItem(new StockItem(1L, "test", "", 2.0, 2), 2);
        assertTrue(cart.canBeAdded(soldItem));
        cart.addItem(soldItem);
        assertTrue(cart.alreadyInCart(soldItem));
    }

    @Test
    public void testAddingItemWithNegativeQuantity() {
        try {
            SoldItem soldItem = new SoldItem(new StockItem(1L, "test", "", -2.0, 2), 2);
            cart.addItem(soldItem);
            if (cart.alreadyInCart(soldItem))
                assertTrue(false);
        } catch (Exception e) {
            assertTrue(true);
        }
    }

    @Test
    public void testAddingItemWithQuantityTooLarge() {
        try {
            SoldItem soldItem = new SoldItem(dao.findStockItem(1L), 999);
            cart.addItem(soldItem);
            if (cart.alreadyInCart(soldItem))
                assertTrue(false);
        } catch (Exception e) {
            assertTrue(true);
        }
    }

    @Test
    public void testAddingItemWithQuantitySumTooLarge() {
        try {
            cart.addItem(new SoldItem(dao.findStockItem(1L), 5));
            cart.addItem(new SoldItem(dao.findStockItem(1L), 5));
            assertTrue(false);
        } catch (Exception e) {
            assertTrue(true);
        }
    }
}
